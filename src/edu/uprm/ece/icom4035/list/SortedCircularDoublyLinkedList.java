package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SortedCircularDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> 
{
	//instance variables
	private Node<E> dummyHeader; 
	private int currentSize;

	
	//constructor
	public SortedCircularDoublyLinkedList()
	{
		dummyHeader = new Node<E>();
		dummyHeader.setNext(dummyHeader);
		dummyHeader.setPrev(dummyHeader);
		currentSize = 0;
	}

	//Methods 
	@Override
	public boolean add(E obj) 
	{
		Node<E> nodeToAdd = new Node<E>(obj);
		Node<E> currNode = this.dummyHeader.getNext();
		
		if(this.currentSize == 0)
		{	
			this.dummyHeader.setNext(nodeToAdd);	
			nodeToAdd.setPrev(dummyHeader);
			nodeToAdd.setNext(dummyHeader);
			this.dummyHeader.setPrev(nodeToAdd);
		}
		
		else
		{
			for(int i=0; i < this.currentSize; i++)
			{
				if(obj.compareTo(currNode.getData()) < 0)
				{						
					nodeToAdd.setNext(currNode);
					nodeToAdd.setPrev(currNode.getPrev());
					currNode.getPrev().setNext(nodeToAdd);
					currNode.setPrev(nodeToAdd);
					this.currentSize++;
					return true;

				}

				//"equals get inserted at the first available spot" -project documentation
				else if((obj.equals(currNode.getData())))

				{	
					nodeToAdd.setNext(currNode);
					nodeToAdd.setPrev(currNode.getPrev());
					currNode.getPrev().setNext(nodeToAdd);
					currNode.setPrev(nodeToAdd);
					this.currentSize++;
					return true;

				}

				else if((obj).compareTo(currNode.getData()) > 0)
				{	if(currNode.getNext() == this.dummyHeader)
					{
						nodeToAdd.setNext(dummyHeader);
						nodeToAdd.setPrev(currNode);
						currNode.setNext(nodeToAdd);
						dummyHeader.setPrev(nodeToAdd);
					}
					else{	currNode = currNode.getNext();	}
				
				}
			}
		}
		
		
		this.currentSize++;
		return true;
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		if(!this.contains(obj))
			return false;
		
		else 
		{
			Node<E> currNode = this.dummyHeader.getNext();

			for(int i=0; i < this.currentSize; i++)
			{
				if(currNode.getData().equals(obj))
				{	
					this.remove(i);
					return true;
				}

				else {	currNode = currNode.getNext();	}
			}
		}
		return false;  //dummy. Never will be reached due to this.contains(obj).
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index > this.currentSize)
		{	throw new IndexOutOfBoundsException("remove Error: Invalid index.");	}		
		
		Node<E> nodeToRemove = this.dummyHeader.getNext();
		
		for(int i=0; i < index; i++)
		{	nodeToRemove = nodeToRemove.getNext();	}
		
		nodeToRemove.getNext().setPrev(nodeToRemove.getPrev());
		nodeToRemove.getPrev().setNext(nodeToRemove.getNext());
		
		//Helping Garbage Collector 
		nodeToRemove.cleanLinks();
		nodeToRemove.setData(null);
		
		this.currentSize--;
		
		return true;
	}

	@Override
	public int removeAll(E obj) 
	{
		int numOfCopies = 0;

		if(!this.contains(obj))
			return numOfCopies;
		
		Node<E> currNode = this.dummyHeader.getNext();
		
		while(currNode != this.dummyHeader)
		{
			if(currNode.getData().equals(obj))
			{	
				currNode = currNode.getNext();
				this.remove(obj);
				numOfCopies++;
			}
			
			else
			{	currNode = currNode.getNext();	}
		}
		
		return numOfCopies;
	}

	@Override
	public E first() {
		if(this.isEmpty())
			return null;
			
		return this.dummyHeader.getNext().getData();
	}

	@Override
	public E last() {
		if(this.isEmpty())
			return null;
			
		return this.dummyHeader.getPrev().getData();
	}

	@Override
	public E get(int index) 
	{
		if(index < 0 || index > this.currentSize)
		{	throw new IndexOutOfBoundsException("get Error: Invalid index.");	}
		
		Node<E> currNode = this.dummyHeader.getNext();
		
		for(int i=0; i < index; i++)
		{	currNode = currNode.getNext();	}

		return currNode.getData();
	}

	@Override
	public void clear() 
	{
		Node<E> currNode = this.dummyHeader.getNext();
		
		for(int i=0; i < this.currentSize; i++)
		{	
			currNode.setData(null);
			currNode = currNode.getNext(); //maybe problematic 
			currNode.getPrev().cleanLinks();
		}
		
		//Once all nodes are deleted dummyHeader is set back to a empty list format. 
		dummyHeader.setNext(dummyHeader);
		dummyHeader.setPrev(dummyHeader);
		currentSize = 0;
		
	}

	@Override
	public boolean contains(E e) 
	{
		Node<E> currNode = this.dummyHeader.getNext();
		
		for(int i=0; i < this.currentSize; i++)
		{
			if(currNode.getData().equals(e))
				return true;
			currNode = currNode.getNext();
		}

		return false;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}
	
	@Override
	public int firstIndex(E e) {
		if(!this.contains(e))
			return -1;
		
		Node<E> currNode = this.dummyHeader.getNext();
		for(int i=0; i < this.currentSize; i++)
		{
			if(currNode.getData().equals(e))
				return i;
			
			currNode = currNode.getNext();
		}
		return -1; //dummy. Never will be reached due to this.contains(e).
	}
	
	@Override
	public int lastIndex(E e) {
		if(!this.contains(e))
			return -1;

		Node<E> currNode = this.dummyHeader.getPrev();
		for(int i=this.currentSize-1; i >= 0; i--)
		{
			if(currNode.getData().equals(e))
				return i;
			
			currNode = currNode.getPrev();
		}
		return -1; //dummy. Never will be reached due to this.contains(e).
	}
	
	@Override
	public Iterator<E> iterator(int index) 
	{	return new ForwardCircularDLListIterator(index);	}

	//Method not in project documentation but it is a method inherited from Iterable. 
	@Override
	public Iterator<E> iterator() {
		return new ForwardCircularDLListIterator();
	}

	@Override
	public ReverseIterator<E> reverseIterator() {
		return new ReverseCircularDLListIterator();
	}

	@Override
	public ReverseIterator<E> reverseIterator(int index) {
		return new ReverseCircularDLListIterator(index);
	}
	
	//Internal Auxiliary Classes for the Iterators (forward and reverse) 
	private class ReverseCircularDLListIterator implements ReverseIterator<E>
	{
		private Node<E> cursor = dummyHeader.getNext();
		private Node<E> recent = null;

		//constructors
		public ReverseCircularDLListIterator()
		{	cursor = dummyHeader.getPrev();	}

		public ReverseCircularDLListIterator(int index)
		{
			
			if(index < 0 || index > currentSize)
			{	throw new IndexOutOfBoundsException("Reverse Iterator Error: Invalid index.");	}


			for(int i=0; i < index; i++)
				cursor = cursor.getNext(); //cursor is on element of position index. 
				
		}

		@Override
		public boolean hasPrevious() {
			return cursor != dummyHeader;
		}

		@Override
		public E previous() 
		{
			if(!hasPrevious()) throw new NoSuchElementException("Iterator is completed");
			
			recent = cursor;
			cursor = cursor.getPrev();
			
			return recent.data;
		}
		
	}
	
	private class ForwardCircularDLListIterator implements Iterator<E>
	{	
		private Node<E> cursor = dummyHeader.getNext();
		private Node<E> recent = null;

		//constructor with parameter index. IOOBE if not valid index. 
		public ForwardCircularDLListIterator(int index)
		{
			if(index < 0 || index > currentSize)
			{	throw new IndexOutOfBoundsException("Forward Iterator Error: Invalid index.");	}

			for(int i=0; i< index; i++)
				cursor = cursor.getNext(); //cursor is on element of position index. 
				
		}
		
		public ForwardCircularDLListIterator() {
			new ForwardCircularDLListIterator(0);
		}

		@Override
		public boolean hasNext() {
			return cursor != dummyHeader;
		}

		@Override
		public E next() {
			if(!hasNext()) throw new NoSuchElementException("Iterator is completed");
						
			recent = cursor;
			cursor = cursor.getNext();
			
			return recent.data;
		}
		
	}
	
	//Internal Node Class for the implementation of DoublyLinkedList with sentinel (dummy) header.
	private static class Node<E> 
	{
		private E data; 
		private Node<E> prev, next; 

		// Constructors
		public Node() {}
		
		public Node(E e) { 
			data = e; 
		}
		
		public Node(E e, Node<E> p, Node<E> n) { 
			prev = p; 
			next = n; 
		}
		
		// Methods
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) { 
			this.prev = prev;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public E getData() {
			return data; 
		}

		public void setData(E data) {
			data = data; 
		} 
		
		public void cleanLinks() { 
			prev = next = null; 
		}
		
	}

}
